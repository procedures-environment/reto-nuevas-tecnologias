
############################################################################################################################
############################################################################################################################
### Kind installation

==================================================================
== Disable crypto policies
==================================================================

# update-crypto-policies --show
# update-crypto-policies --set DEFAULT:SHA1


# dnf group install "Container Management" -y && dnf install epel-release.noarch curl git wget neovim htop make -y
# dnf install podman podman-compose podman-docker podman-plugins podman-remote skopeo buildah -y




==================================================================
== Prerequisites
==================================================================

# for i in ansiblewme; do useradd -c "Ansible User Automation" --uid 1000 $i && echo "123456" | passwd --stdin $i; done
# visudo


# sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config && cat /etc/selinux/config | grep ^SELINUX=


# systemctl disable --now firewalld.service
# systemctl mask firewalld.service
# systemctl status firewalld.service




==================================================================
== Enable rootless for kind
==================================================================

# podman info | grep -E 'cgroupVersion'
cgroupVersion: v2

# mkdir -v /etc/systemd/system/user@.service.d/

# vim /etc/systemd/system/user@.service.d/delegate.conf
[Service]
Delegate=yes

# systemctl daemon-reload




==================================================================
== iptables roles for kind
==================================================================

# vim /etc/modules-load.d/iptables.conf
ip6_tables
ip6table_nat
ip_tables
iptable_nat

# systemctl reboot




==================================================================
== kind Installation
==================================================================

$ [ $(uname -m) = x86_64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.22.0/kind-linux-amd64
$ chmod +x ./kind
$ sudo install ./kind /usr/local/bin/kind
$ kind --version




==================================================================
== kubectl Installation
==================================================================

$ KUBECTL_VERSION="v1.29.2"
$ curl -LO "https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl"
$ curl -LO "https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl"


$ curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
$ curl -LO "https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl"


$ chmod +x ./kubectl
$ sudo install ./kubectl /usr/local/bin/kubectl
$ kubectl version -o yaml
$ kubectl version --client

