# Reto Nuevas Tecnologias

Se deja guia de imprementación de nuevas tecnologías.

- El sistema operativo usado es Rocky Linux 9.3.
- Rocky Linux 9.x ya viene con Cgroupv2 por defecto, esto permite correr procesos en rootless.

## Kubernetes

- Kind soporta rootless.
- Instalación kind: Se implementa en modo rootless para que los procesos esten corriendo con un usuario no privilegiado.

## Contenedores

- Se ha usado podman ya que podman soporta modo rootless. Esto evita que los procesos corran con usuario root.
- Podman soporta Cgroupv2.

## LVM

- Se apñade nuevo disco sda.
- Se implementa los PVs, VGs, LVs.

## Networking

- Se añade interfaz de red adicional.
- Se usa la tool nmcli para configurar la interfaz de red estática.

## Ansible

- Se desarrolla el playbook y se correo en localhost para la desinstalación e instalación de paquetes rpm.

